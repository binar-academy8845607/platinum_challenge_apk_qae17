package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class RespondToOffer {

	@Given("User navigates to notification page for accepting offers")
	def navigatesToAcceptOffer() {
		Mobile.startApplication('D:\\binar\\clone mobile apk\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunketiga@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/TextView - Daftar Jual Saya'), 0)
	}

	@When("User accept an offer")
	def acceptOffer() {

		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/TextView - Diminati'), 0)

		Mobile.tap(findTestObject('Page_Respond to Offer/Page_tawaran/TextView - Ditawar Rp10,000 - accept'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/Button - Terima'), 0)
	}

	@Then("User on notification page after accepting")
	def verifyAccept() {
		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/Button - Hubungi via Whatsapp'), 0)

		Mobile.closeApplication()
	}

	@Given("User navigates to notification page for rejecting offers")
	def navigatesToRejectOffer() {
		Mobile.startApplication('D:\\binar\\clone mobile apk\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunketiga@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/TextView - Daftar Jual Saya'), 0)
	}

	@When("User reject an offer")
	def rejectOffer() {
		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/TextView - Diminati'), 0)

		Mobile.tap(findTestObject('Page_Respond to Offer/Page_tawaran/TextView - Ditawar Rp50,000 - reject'), 0)

		Mobile.tap(findTestObject('Page_Respond to Offer/Page_Tolak/Button - Tolak'), 0)
	}

	@Then("User on notification page after rejecting")
	def verifyReject() {
		Mobile.verifyElementVisible(findTestObject('Page_Respond to Offer/Page_Tolak/ViewGroup - verify element'), 0)

		Mobile.closeApplication()
	}

	@Given("User navigates to notification page for contacting via WA")
	def navigatesToContactWa() {
		Mobile.startApplication('D:\\binar\\clone mobile apk\\platinum_challenge_apk_qae17\\APK\\secondhand.apk', true)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/TextView_Akun'), 0)

		Mobile.tap(findTestObject('Page_Add Product/Page_spy/Btn - Masuk awal'), 0)

		Mobile.setText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan email'), 'akunketiga@abcmail.com', 0)

		Mobile.setEncryptedText(findTestObject('Page_Add Product/Page_spy/EditText - Masukkan password'), '3lfpvPgNwYrqMVHiKwftSg==', 0)

		Mobile.tap(findTestObject('Page_Login/Btn_Masuk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/TextView - Daftar Jual Saya'), 0)
	}
	@When("User accept an offer and contact via WA")
	def contactWA() {
		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/TextView - Diminati'), 0)

		Mobile.tap(findTestObject('Page_Respond to Offer/Page_tawaran/TextView - Ditawar Rp200,000 - wa'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/Button - Terima'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Respond to Offer/Page_Record/Button - Hubungi via Whatsapp'), 0)
	}
	@Then("User on WA Page")
	def verifyWa() {
		Mobile.takeScreenshot('C:\\screenshot binar\\waclick.png', FailureHandling.STOP_ON_FAILURE)

		Mobile.closeApplication()
	}
}