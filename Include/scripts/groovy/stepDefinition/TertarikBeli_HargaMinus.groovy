package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When




class TertarikBeli_HargaMinus {

	@Given("users search for items")
	def search_item() {
		Mobile.startApplication('C:\\Users\\ASUS\\Downloads\\secondhand-24082023.apk', true)
	}

	@When("Users negotiate prices")
	def user_nego() {
		Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Click_Akun'), 0)

		Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Click - Masuk'), 0)

		Mobile.setText(findTestObject('Page_Tertarik dan Nego Harga/EditText - Masukkan email'), 'yogadimassetyo1@gmail.com', 0)

		Mobile.setText(findTestObject('Page_Tertarik dan Nego Harga/EditText - Masukkan password'), 'yogadimas123', 0)

		Mobile.pressBack()

		Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Btn_Masuk Login'), 0)

		Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Click_Beranda'), 0)

		Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Klick_ -'), 0)

		Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Button - Saya Tertarik dan Ingin Nego'), 0)

		Mobile.setText(findTestObject('Page_Tertarik dan Nego Harga/EditText - Rp 0,00'), '-1000000', 0)

		Mobile.pressBack()

		Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Button - Kirim'), 0)
	}

	@Then("Waiting for a reply from the seller")
	def waiting_seller() {
		Mobile.verifyElementVisible(findTestObject('Page_Tertarik dan Nego Harga/android.view.Succes'), 0)

		Mobile.closeApplication()
	}
}