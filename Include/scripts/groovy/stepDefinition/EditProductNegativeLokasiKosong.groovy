package stepDefinition
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProductNegativeLokasiKosong {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User navigates to edit product blank lokasi barang")
	def navigateToeditnegativepage2() {
		Mobile.startApplication('C:\\Binar Academy\\Platinum Challenge\\secondhand-01022024.apk', true)

		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/btn_akun_awal produk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/btn_masuk produk'), 0)

		Mobile.setText(findTestObject('Object Repository/Page_Edit Produk/Masukkan email edit produk'), 'overlord.njoo@gmail.com',
				0)

		Mobile.setText(findTestObject('Object Repository/Page_Edit Produk/Masukkan password edit produk'), '31107010', 0)

		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Button - Masuk produk'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Daftar Jual Saya'), 0)
	}

	@When("User fill form edit product blank lokasi barang")
	def entereditproductnegativelokasi() {
		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/ViewGroup produk 1'), 0)

		Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

		Mobile.setText(findTestObject('Page_Edit Produk/txt_lokasi_kosong'), '', 0)

		Mobile.verifyElementVisible(findTestObject('Page_Edit Produk/validasi_Lokasi tidak boleh kosong'), 0)

		Mobile.tap(findTestObject('Object Repository/Page_Edit Produk/Btn_update_produk'), 0)
	}

	@Then("User is form edit product2")
	def verifylisteditproductnegative2() {
		Mobile.closeApplication()
	}
}