Feature: Add a product with empty price

Scenario: User want to add a product with empty price
Given User navigates to add product page with empty price
When User fills the form without input price
Then User get a notification to input the price
    