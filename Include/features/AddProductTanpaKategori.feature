Feature: Add a product without categories

Scenario: User want to add a product without categories
Given User navigates to add product page without categories
When User fills the form without input categories
Then User get a notification to input categories
    
    