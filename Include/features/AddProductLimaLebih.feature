Feature: Add more than five products

Scenario: User want to add first product
Given User navigates to add product page to add first product
When User fills the form to add first product
Then User on a product page after add first product

Scenario: User want to add second product
Given User navigates to add product page to add second product
When User fills the form to add second product
Then User on a product page after add second product

Scenario: User want to add third product
Given User navigates to add product page to add third product
When User fills the form to add third product
Then User on a product page after add third product

Scenario: User want to add fourth product
Given User navigates to add product page to add fourth product
When User fills the form to add fourth product
Then User on a product page after add fourth product

Scenario: User want to add fifth product
Given User navigates to add product page to add fifth product
When User fills the form to add fifth product
Then User on a product page after add fifth product

Scenario: User want to add sixth product
Given User navigates to add product page to add sixth product
When User fills the form to add sixth product
Then User on a product page after add sixth product