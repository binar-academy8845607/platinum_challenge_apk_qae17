Feature: Respond to Offer Feature

Scenario: User wants to accept an offer
	Given User navigates to notification page for accepting offers
	When User accept an offer
	Then User on notification page after accepting
	
Scenario: User wants to reject an offer
	Given User navigates to notification page for rejecting offers
	When User reject an offer
	Then User on notification page after rejecting
	
Scenario: User wants to give accept offer and contact buyer via WA
	Given User navigates to notification page for contacting via WA
	When User accept an offer and contact via WA
	Then User on WA Page
