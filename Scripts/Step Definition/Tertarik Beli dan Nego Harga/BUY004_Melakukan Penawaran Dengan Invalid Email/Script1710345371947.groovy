import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\ASUS\\Downloads\\secondhand-24082023.apk', true)

Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Click_Akun'), 0)

Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Click - Masuk'), 0)

Mobile.setText(findTestObject('Page_Tertarik dan Nego Harga/EditText - Masukkan email'), 'aaa', 0)

Mobile.setText(findTestObject('Page_Tertarik dan Nego Harga/EditText - Masukkan password'), 'hhhhhhhh', 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Page_Tertarik dan Nego Harga/Btn_Masuk Login'), 0)

Mobile.verifyElementVisible(findTestObject('Page_Tertarik dan Nego Harga/android.widget.TextView - Email tidak valid'), 
    0)

Mobile.closeApplication()

