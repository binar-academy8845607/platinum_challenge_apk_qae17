$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/binar/clone mobile apk/platinum_challenge_apk_qae17/Include/features/AddProductTanpaKategori.feature");
formatter.feature({
  "name": "Add a product without categories",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to add a product without categories",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page without categories",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductTanpaKategori.navigatesToAddProductWithoutCategory()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form without input categories",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductTanpaKategori.fillFormsWithoutCategory()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User get a notification to input categories",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductTanpaKategori.verifyWithoutCategory()"
});
formatter.result({
  "status": "passed"
});
});