$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("D:/binar/clone mobile apk/platinum_challenge_apk_qae17/Include/features/AddProductLimaLebih.feature");
formatter.feature({
  "name": "Add more than five products",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to add first product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page to add first product",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductLimaLebih.navigatesToAddFirstProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form to add first product",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductLimaLebih.fillFormsFirstProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on a product page after add first product",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductLimaLebih.verifyFirstProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User want to add second product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page to add second product",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductLimaLebih.navigatesToAddSecondProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form to add second product",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductLimaLebih.fillFormsSecondProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on a product page after add second product",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductLimaLebih.verifySecondproduct()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User want to add third product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page to add third product",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductLimaLebih.navigateToAddThirdProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form to add third product",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductLimaLebih.fillFormThirdProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on a product page after add third product",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductLimaLebih.verifyThirdProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User want to add fourth product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page to add fourth product",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductLimaLebih.navigatesToAddFourthProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form to add fourth product",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductLimaLebih.fillFormsFourthProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on a product page after add fourth product",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductLimaLebih.verifyFourthProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User want to add fifth product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page to add fifth product",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductLimaLebih.navigatesToFifthProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form to add fifth product",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductLimaLebih.fillFormsFifthProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on a product page after add fifth product",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductLimaLebih.verifyFifthProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User want to add sixth product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to add product page to add sixth product",
  "keyword": "Given "
});
formatter.match({
  "location": "AddProductLimaLebih.navigatesToSixthProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fills the form to add sixth product",
  "keyword": "When "
});
formatter.match({
  "location": "AddProductLimaLebih.fillFormsSixthProduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User on a product page after add sixth product",
  "keyword": "Then "
});
formatter.match({
  "location": "AddProductLimaLebih.verifySixthProduct()"
});
formatter.result({
  "status": "passed"
});
});