$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/USER/Katalon Studio/APK_Second_Hand/Include/features/LogOut.feature");
formatter.feature({
  "name": "Logout Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to logout",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User already to login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LogoutStep.navigateToLogin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click logout",
  "keyword": "When "
});
formatter.match({
  "location": "LogoutStep.entercredentialslogout()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is navigated on home",
  "keyword": "Then "
});
formatter.match({
  "location": "LogoutStep.verifyhome()"
});
formatter.result({
  "status": "passed"
});
});