$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Binar Academy/Platinum Challenge/platinum_challenge_apk_qae17/Include/features/EditProduct.feature");
formatter.feature({
  "name": "Edit Product Feature",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User want to Edit Product",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User navigates to edit product",
  "keyword": "Given "
});
formatter.match({
  "location": "EditProduct.navigateToeditpage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fill form edit product",
  "keyword": "When "
});
formatter.match({
  "location": "EditProduct.entereditproduct()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User is navigated list product",
  "keyword": "Then "
});
formatter.match({
  "location": "EditProduct.verifylisteditproduct()"
});
formatter.result({
  "status": "passed"
});
});